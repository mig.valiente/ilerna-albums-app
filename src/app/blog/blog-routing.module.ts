import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PostsListPageComponent} from './pages/posts-list-page/posts-list-page.component';
import {PostDetailPageComponent} from './pages/post-detail-page/post-detail-page.component';

const routes: Routes = [
  {path: '', component: PostsListPageComponent},
  {path: 'post/:id', component: PostDetailPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BlogRoutingModule {}
