import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsListPageComponent } from './pages/posts-list-page/posts-list-page.component';
import { PostDetailPageComponent } from './pages/post-detail-page/post-detail-page.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BlogRoutingModule} from './blog-routing.module';



@NgModule({
  declarations: [
    PostsListPageComponent,
    PostDetailPageComponent
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    FontAwesomeModule
  ]
})
export class BlogModule { }
