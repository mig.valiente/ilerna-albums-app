import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostsService} from '../../posts.service';
import {ActivatedRoute} from '@angular/router';
import {pluck, switchMap, takeUntil} from 'rxjs/operators';
import {Post} from '../../models/post';
import {Subject} from 'rxjs';
import {faUserCircle, IconDefinition} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-post-detail-page',
  templateUrl: './post-detail-page.component.html',
  styleUrls: ['./post-detail-page.component.scss']
})
export class PostDetailPageComponent implements OnInit, OnDestroy {

  post: Post | undefined;
  userIcon: IconDefinition = faUserCircle;

  private destroy$ = new Subject<boolean>()

  constructor(private route: ActivatedRoute, private postsSrv: PostsService) {
    this.route.params.pipe(
      takeUntil(this.destroy$),
      pluck('id'),
      switchMap(id => this.postsSrv.fetchPost(id))
    ).subscribe(post => this.post = post);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
