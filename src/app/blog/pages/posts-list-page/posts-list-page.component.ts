import { Component, OnInit } from '@angular/core';
import {PostsService} from '../../posts.service';
import {Post} from '../../models/post';

@Component({
  selector: 'app-posts-list-page',
  templateUrl: './posts-list-page.component.html',
  styleUrls: ['./posts-list-page.component.scss']
})
export class PostsListPageComponent implements OnInit {

  posts: Post[] = [];

  constructor(private postsService: PostsService) {
    this.postsService.fetchPosts().subscribe(posts => {
      this.posts = posts;
    });
  }

  ngOnInit(): void {
  }

}
