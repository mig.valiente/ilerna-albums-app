import {Inject, Injectable} from '@angular/core';
import {API_URL} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {combineLatest, Observable} from 'rxjs';
import {Post} from './models/post';
import {Comment} from './models/comment';
import {map, mapTo, switchMap, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private readonly postEndpoint = 'posts'
  private readonly commentsEndpoint = 'comments'
  private readonly postsRequestUrl: string;
  private readonly commentsRequestUrl: string;

  constructor(@Inject(API_URL) private api_url: string, private http: HttpClient) {
    this.postsRequestUrl = api_url + this.postEndpoint;
    this.commentsRequestUrl = api_url + this.commentsEndpoint;
  }

  fetchPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsRequestUrl);
  }

  fetchPost(postId: number): Observable<Post> {
    const postRequest = this.http.get<Post>(`${this.postsRequestUrl}/${postId}`);
    return combineLatest([postRequest, this.fetchPostComments(postId)]).pipe(
      map(([post, comments]) => {
        post.comments = comments;
        return post;
      })
    )
  }

  private fetchPostComments(postId: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.commentsRequestUrl}?postId=${postId}`);
  }
}
