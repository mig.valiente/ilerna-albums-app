import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {pluck, switchMap, takeUntil, tap} from 'rxjs/operators';
import {PhotosService} from '../../photos.service';
import {Photo} from '../../models/photo';
import {Subject} from 'rxjs';
import {AlbumsService} from '../../albums.service';
import {Album} from '../../models/album';
import {MatDialog} from '@angular/material/dialog';
import {PhotoModalComponent} from '../../view-components/photo-modal/photo-modal.component';

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.scss']
})
export class AlbumPageComponent implements OnInit, OnDestroy {

  album: Album | undefined;
  photos: Photo[] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private route: ActivatedRoute,
    private albumsService: AlbumsService,
    private photosService: PhotosService,
    private dialog: MatDialog
  ) {
    route.params.pipe(
      takeUntil(this.destroy$),
      pluck('albumId'),
      switchMap(albumId => this.albumsService.fetchAlbum(albumId)),
      tap(album => this.album = album),
      switchMap(album => photosService.fetchPhotos(album.id))
    ).subscribe(photos => this.photos = photos);
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  viewPhoto(photo: Photo) {
    const dialogRef = this.dialog.open(PhotoModalComponent, {
      data: photo
    })
  }
}
