import {Inject, Injectable} from '@angular/core';
import {API_URL} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Album} from './models/album';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  private readonly endpoint = 'albums'
  private readonly requestUrl: string;

  constructor(@Inject(API_URL) private api_url: string, private http: HttpClient) {
    this.requestUrl = api_url + this.endpoint;
  }

  fetchAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(this.requestUrl);
  }

  fetchAlbum(albumId: number): Observable<Album> {
    return this.http.get<Album>(`${this.requestUrl}/${albumId}`);
  }

  fetchAlbumsByUserId(userId: number): Observable<Album[]> {
    return this.http.get<Album[]>(`${this.requestUrl}?userId=${userId}`);
  }
}
