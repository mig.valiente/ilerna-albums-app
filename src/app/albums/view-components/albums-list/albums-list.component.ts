import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Album} from '../../models/album';
import {faImages, IconDefinition} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.scss']
})
export class AlbumsListComponent implements OnInit {

  @Input() albums: Album[] = [];
  @Output() selectedAlbum: EventEmitter<Album> = new EventEmitter<Album>();

  albumIcon: IconDefinition = faImages

  constructor() { }

  ngOnInit(): void {
  }

}
