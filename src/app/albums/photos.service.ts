import {Inject, Injectable} from '@angular/core';
import {API_URL} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {Photo} from './models/photo';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  private readonly endpoint = 'photos'
  private readonly requestUrl: string;

  constructor(@Inject(API_URL) private api_url: string, private http: HttpClient) {
    this.requestUrl = api_url + this.endpoint;
  }

  fetchPhotos(albumId: number): Observable<Photo[]> {
    return this.http.get<Photo[]>(`${this.requestUrl}?albumId=${albumId}`);
  }
}
