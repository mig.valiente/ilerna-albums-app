import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { AlbumsListComponent } from './view-components/albums-list/albums-list.component';
import {MatCardModule} from '@angular/material/card';
import { AlbumPageComponent } from './pages/album-page/album-page.component';
import {RouterModule} from '@angular/router';
import {MatGridListModule} from '@angular/material/grid-list';
import { PhotoModalComponent } from './view-components/photo-modal/photo-modal.component';
import {MatDialogModule} from '@angular/material/dialog';



@NgModule({
  declarations: [
    AlbumsListComponent,
    AlbumPageComponent,
    PhotoModalComponent
  ],
  exports: [
    AlbumsListComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    FontAwesomeModule,
    RouterModule,
    MatGridListModule,
    MatDialogModule
  ]
})
export class AlbumsModule { }
