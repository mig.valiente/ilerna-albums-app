import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../models/user';
import {pluck, skipWhile, switchMap, takeUntil, tap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Album} from '../../../albums/models/album';
import {AlbumsService} from '../../../albums/albums.service';

@Component({
  selector: 'app-user-detail-page',
  templateUrl: './user-detail-page.component.html',
  styleUrls: ['./user-detail-page.component.scss']
})
export class UserDetailPageComponent implements OnInit, OnDestroy {

  user$: Observable<User>;
  private userUpdated$: Subject<number> = new Subject<number>();
  albums: Album[] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private usersService: UsersService,
    private albumsService: AlbumsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.user$ = route.params.pipe(
      takeUntil(this.destroy$),
      pluck('id'),
      switchMap(id => this.usersService.fetchUser(id)),
      tap(user => this.userUpdated$.next(user.id))
    );

    this.userUpdated$.pipe(
      takeUntil(this.destroy$),
      switchMap(userId => this.albumsService.fetchAlbumsByUserId(userId))
    ).subscribe(albums => this.albums = albums);
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  onSelectAlbum(album: Album) {
    this.router.navigate(['albums', album.id], {relativeTo: this.route});
  }
}
