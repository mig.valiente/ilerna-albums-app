import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/user';
import {Observable} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.scss']
})
export class UsersListPageComponent implements OnInit {

  users$: Observable<User[]>;
  selectedUser: User | null = null;
  columnsToDisplay = ['id', 'userName', 'name', 'email'];

  constructor(private usersService: UsersService, private router: Router, private route: ActivatedRoute) {
    this.users$ = this.usersService.fetchUsers();
  }

  ngOnInit(): void {}

  goToDetail() {
    const userId = this.selectedUser ? this.selectedUser.id : null;
    this.router.navigate(['.', userId], {relativeTo: this.route})
  }
}
