import {Inject, Injectable} from '@angular/core';
import {API_URL} from '../../app.module';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private readonly endpoint = 'users'
  private readonly requestUrl: string;

  constructor(@Inject(API_URL) private api_url: string, private http: HttpClient) {
    this.requestUrl = api_url + this.endpoint;
  }

  fetchUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.requestUrl);
  }

  fetchUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.requestUrl}/${id}`);
  }
}
