import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../models/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Input() users: User[] = [];
  @Output() userSelected: EventEmitter<User | null> = new EventEmitter<User | null>();
  selected: User | null = null;

  columnsToDisplay = ['id', 'userName', 'name', 'email', 'phone'];

  constructor() { }

  ngOnInit(): void {
  }

  selectUser(user: User): void {
    if (this.selected && this.selected === user) {
      this.selected = null;
    } else {
      this.selected = user;
    }
    this.userSelected.emit(this.selected);
  }
}
