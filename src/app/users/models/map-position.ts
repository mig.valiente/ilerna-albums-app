export interface MapPosition {
  lat: string;
  lng: string;
}
