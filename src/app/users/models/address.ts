import {MapPosition} from './map-position';

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: MapPosition;
}
