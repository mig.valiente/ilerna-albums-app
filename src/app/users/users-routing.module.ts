import {RouterModule, Routes} from '@angular/router';
import {UsersListPageComponent} from './pages/users-list-page/users-list-page.component';
import {UserDetailPageComponent} from './pages/user-detail-page/user-detail-page.component';
import {AlbumPageComponent} from '../albums/pages/album-page/album-page.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: '', component: UsersListPageComponent},
  {path: ':id', component: UserDetailPageComponent},
  {path: ':userId/albums/:albumId', component: AlbumPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UsersRoutingModule  {}


