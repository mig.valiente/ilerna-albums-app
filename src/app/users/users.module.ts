import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersListComponent} from './view-components/users-list/users-list.component';
import {UsersListPageComponent} from './pages/users-list-page/users-list-page.component';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {UserDetailPageComponent} from './pages/user-detail-page/user-detail-page.component';
import {RouterModule} from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {UserInfoComponent} from './view-components/user-info/user-info.component';
import {AlbumsModule} from '../albums/albums.module';
import {UsersRoutingModule} from './users-routing.module';


@NgModule({
  declarations: [
    UsersListComponent,
    UsersListPageComponent,
    UserDetailPageComponent,
    UserInfoComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatCardModule,
    UsersRoutingModule,

    AlbumsModule
  ],
  exports: [
    RouterModule
  ]
})
export class UsersModule { }
