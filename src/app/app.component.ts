import { Component } from '@angular/core';
import {UsersService} from './users/services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'albums';

  constructor() {
  }

}
